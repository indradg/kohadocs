# Compendium of es.
msgid ""
msgstr ""
"Project-Id-Version: compendium-es\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-10-25 12:27-0300\n"
"PO-Revision-Date: 2017-11-27 21:52+0000\n"
"Last-Translator: Bernardo <bgkriegel@gmail.com>\n"
"Language-Team: Koha Translation Team\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.5.1.1\n"
"X-POOTLE-MTIME: 1511819542.000000\n"

#: ../../source/14_plugins.rst:4
msgid "Plugin System"
msgstr "Sistema de Plugin"

#: ../../source/14_plugins.rst:6
msgid ""
"Koha's Plugin System allows for you to add additional tools and reports to "
"Koha that are specific to your library. Plugins are installed by uploading "
"KPZ ( Koha Plugin Zip ) packages. A KPZ file is just a zip file containing "
"the perl files, template files, and any other files necessary to make the "
"plugin work."
msgstr ""
"El sistema de plugins de Koha le permite agregar herramientas adicionales e "
"informes de Koha que sean específicos para su biblioteca. Los plugins se "
"instalan subiendo paquetes KPZ (Koha Plugin ZIP). Un archivo KPZ es solo un "
"archivo zip que contiene archivos perl, templates, y cualquier otro tipo de "
"archivos necesario para hacer que el plugin funcione."

#: ../../source/14_plugins.rst:12
msgid "The plugin system needs to be turned on by a system administrator."
msgstr ""
"El sistema de plugins tiene que ser activado por el administrador del "
"sistema."

#: ../../source/14_plugins.rst:17
msgid "Set up"
msgstr "Configuración"

#: ../../source/14_plugins.rst:19
msgid ""
"To set up the Koha plugin system you must first make some changes to your "
"install."
msgstr ""
"Para configurar el sistema de plugins de Koha usted debe, primero, hacer "
"algunos cambios en su instalación."

#: ../../source/14_plugins.rst:22
msgid ""
"Change <enable\\_plugins>0</enable\\_plugins> to <enable\\_plugins>1</enable"
"\\_plugins> in your koha-conf.xml file"
msgstr ""
"Cambie <enable\\_plugins>0</enable\\_plugins> a <enable\\_plugins>1</enable\\"
"_plugins> en el archivo koha-conf.xml"

#: ../../source/14_plugins.rst:25
msgid "Restart your webserver"
msgstr "Reiniciar su servidor web"

#: ../../source/14_plugins.rst:27
msgid ""
"Once set up is complete you will need to alter your :ref:`UseKohaPlugins` "
"system preference. On the Tools page you will see the Tools Plugins and on "
"the Reports page you will see the Reports Plugins."
msgstr ""
"Una vez haya completado la instalación necesitará alterar su preferencia del "
"sistema :ref:`UseKohaPlugins`. En la página de Herramientas usted verá la "
"Herramienta plugins y en la página Informes usted verá los Plugins de "
"Informes."

#: ../../source/14_plugins.rst:33
msgid "Search History"
msgstr "Historial de búsqueda"

#: ../../source/14_plugins.rst:35
msgid ""
"If you have your :ref:`EnableSearchHistory` preference set to keep your "
"search history then you can access this information by clicking on your "
"username in the top right of the staff client and choosing 'Search history'."
msgstr ""
"Si ha establecido su preferencia :ref:`EnableSearchHistory` para mantener su "
"historial de búsqueda entonces puede acceder a esta información haciendo "
"clic en su nombre de usuario en la parte superior derecha de la interfaz del "
"personal y seleccionando 'Historial de búsqueda'."

#: ../../source/14_plugins.rst:40
msgid "|image1062|"
msgstr "|image1062|"

#: ../../source/14_plugins.rst:42
msgid "From this page you will see your bibliographic search history"
msgstr "Desde esta página podrás ver tu historial de búsqueda bibliográfica"

#: ../../source/14_plugins.rst:44
msgid "|image1063|"
msgstr "|image1063|"

#: ../../source/14_plugins.rst:46
msgid "And your authority search history."
msgstr "Y su historial de búsqueda de autoridades."

#: ../../source/14_plugins.rst:48
msgid "|image1064|"
msgstr "|image1064|"

#: ../../source/14_plugins.rst:51
msgid "About Koha"
msgstr "Acerca de Koha"

#: ../../source/14_plugins.rst:53
msgid ""
"The 'About Koha' area will give you important server information as well as "
"general information about Koha."
msgstr ""
"'Acerca de Koha' le proporcionará importante información del servidor, así "
"como información general sobre Koha."

#: ../../source/14_plugins.rst:56
msgid "*Get there:* More > About Koha"
msgstr "*Ir a:* Más > Acerca de Koha"

#: ../../source/14_plugins.rst:61
msgid "Server Information"
msgstr "Información del servidor"

#: ../../source/14_plugins.rst:63
msgid ""
"Under the 'Server Information' tab you will find information about the Koha "
"version and the machine you have installed Koha on. This information is very "
"important for debugging problems. When reporting issues to your support "
"provider or to the various other support avenues (mailing lists, chat room, "
"etc), it's always good to give the information from this screen."
msgstr ""
"En la pestaña 'Información del servidor' usted encontrará información acerca "
"de la versión Koha y la máquina donde tiene instalado el Koha. Esta "
"información es muy importante para la corregir errores. Cuando informa de "
"problemas a su proveedor de asistencia técnica o a otras instancias de "
"soporte (listas de correo, chat, etc.), siempre es bueno dar la información "
"de esta pantalla."

#: ../../source/14_plugins.rst:70
msgid "|image1065|"
msgstr "|image1065|"

#: ../../source/14_plugins.rst:75
msgid "Perl Modules"
msgstr "Módulos Perl"

#: ../../source/14_plugins.rst:77
msgid ""
"In order to take advantage of all of the functionalities of Koha, you will "
"need to keep your Perl modules up to date. The 'Perl Modules' tab will show "
"you all of the modules required by Koha, the version you have installed and "
"whether you need to upgrade certain modules."
msgstr ""
"Con el fin de aprovechar todas las funcionalidades de Koha, usted necesitará "
"mantener sus módulos de Perl actualizados. La pestaña 'Módulos de Perl' le "
"mostrará todos los módulos requeridos por Koha, la versión instalada y si se "
"tiene que actualizar algún módulo."

#: ../../source/14_plugins.rst:82
msgid "|image1066|"
msgstr "|image1066|"

#: ../../source/14_plugins.rst:84
#, fuzzy
msgid ""
"Items listed in bold are required by Koha, items highlighted in red are "
"missing completely and items highlighted in yellow simply need to be "
"upgraded."
msgstr ""
"Los ítems señalados en negrita son obligatorios en Koha, los ítems en rojo "
"se echan en falta y los ítems resaltados en amarillo simplemente necesitan "
"ser actualizados."

#: ../../source/14_plugins.rst:91
#, fuzzy
msgid "System Information"
msgstr "Información del sistema"

#: ../../source/14_plugins.rst:93
msgid ""
"This tab will provide you with warnings if you are using system preferences "
"that have since been deprecated or system preferences that you have set "
"without other required preferences"
msgstr ""
"Esta pestaña le proporcionará advertencias si utiliza las preferencias del "
"sistema que ya han quedado en desuso o preferencias del sistema que ha "
"establecido sin otras preferencias requeridas"

#: ../../source/14_plugins.rst:97
msgid "|image1067|"
msgstr ""

#~ msgid "`Set up <#pluginsetup>`__"
#~ msgstr "`Configuración <#pluginsetup>`__"

#, fuzzy
#~ msgid "Search history |image1062|"
#~ msgstr "Historial de búsqueda"

#, fuzzy
#~ msgid "Bibliographic search history |image1063|"
#~ msgstr "Historial de búsqueda bibliográfica"

#, fuzzy
#~ msgid "Authority search history |image1064|"
#~ msgstr "Historial de búsqueda de autoridades"

#, fuzzy
#~ msgid "Server Information on Koha |image1065|"
#~ msgstr "Información del servidor en Koha"

#~ msgid "`Perl Modules <#aboutserverperl>`__"
#~ msgstr "`Módulos Perl <#aboutserverperl>`__"

#~ msgid "`System Information <#aboutsystem>`__"
#~ msgstr "`Información del sistema <#aboutsystem>`__"
